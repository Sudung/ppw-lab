from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Josh Sudung'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,7,26)
npm = 1706039824
hobby = "Swimming"
edu = "University of Indonesia"

friend1_name = 'Millenio Ramadizsa'
friend1_npm = '1706040063'
friend1_hobby = 'Nonton film'
friend1_desc = 'Saya mahasiswa fasilkom yang semangat'

friend2_name = 'Nandhika Prayoga'
friend2_npm = '1706039912'
friend2_hobby = 'I do like read and write any book, love to learn something new, and do creative thing.'
friend2_desc = "I'm a dreamer."
# Create your views here.
def index(request):
    response = {'name': mhs_name, 
				'age': calculate_age(birth_date.year), 
				'npm': npm, 
				'hobby' : hobby, 
				'edu' : edu,
				'friend1_name' : friend1_name,
				'friend1_npm' : friend1_npm,
				'friend1_hobby' : friend1_hobby,
				'friend1_desc' : friend1_desc,
				'friend2_name' : friend2_name,
				'friend2_npm' : friend2_npm,
				'friend2_hobby' : friend2_hobby,
				'friend2_desc' : friend2_desc,
				}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
